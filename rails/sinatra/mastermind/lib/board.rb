class Board
  attr_accessor :pairs, :code
  def initialize
    @pairs = {
      1 => :r,
      2 => :g,
      3 => :y,
      4 => :b,
      5 => :p,
      6 => :c
    }

    @code = generate_code
  end
  
  def display_code
    @code.join('').to_i
  end
  
  def generate_code
    [rand(1..6),rand(1..6),rand(1..6),rand(1..6)].map(&:to_s)
  end
 
  def display(number)
    tmp = ""
    split_number(number).each.with_index do |x, i|
      tmp << "#{@pairs[x.to_i]} "
      if i == 1
        organize_pegs(number).each { |x| tmp << x }
        tmp << " "
      end
    end
    tmp << "\n"
    tmp.split('')
  end
  
  def organize_pegs(number)
    peg_array = []
    if include_number?(number)
      peg_array += write_closed_pegs if matching_index?(number)
      peg_array += write_open_pegs(number)
    end
    return peg_array
  end
  
  def write_open_pegs(number)
    open_pegs = []
    ((@code & split_number(number)).length - write_closed_pegs.length).times do |x|
      open_pegs << "◎"
    end
    return open_pegs
  end
 
  def include_number?(number)
    unless (@code & split_number(number)).empty?
      return true
    end
    return false
  end
  
  def share_index?(element, index)
    @code.index(element) == index
  end

  def matching_index?(number)
    @tmp = []
    split_number(number).each.with_index do |x, i|
      if @code[i].include?(x)
        @tmp << x
      else
        @tmp << nil
      end
    end
    #p @tmp
    if @tmp.compact.empty?
      return false
    end
    return true
  end
  
  def write_closed_pegs
    closed_pegs = []
    @tmp.each.with_index do |element, index| 
      closed_pegs << "◉" if share_index?(element, index)
    end
    return closed_pegs
  end

  def split_number(number)
    number.to_s.split('')
  end
end
module Mastermind
  require_relative 'board'
  require_relative 'game'
  require_relative 'player'
end
class Game
  attr_accessor :turn, :history, :board, :guess, :answer, :peg_history, :message
  def initialize
    @board =  Board.new
    @turn = 9
    @history = []
    @peg_history = []
    @guess = nil
    @answer = @board.display_code.to_s.split('').map(&:to_i)
    @message = nil
  end
  
  def turn_decrease
    @turn -= 1
  end
  
  def game_over?
    if lost?
      @message = "You Lost!!"
      return true
    end
    
    if victory?
      @message = "You Won!!"
      return true
    end
    return false
  end
  
  def lost?
    return true if @turn <= 0
  end
  
  def victory?
    @guess == @answer
  end
  
  def reset
    initialize
  end
end
require 'sinatra'
require 'sinatra/reloader'
require_relative 'lib/mastermind'

configure do
  enable :sessions
end

get '/' do
  session[:game] ||= Game.new
  session[:game].reset if session[:game].game_over?

  session[:game].guess = [params["1"], params["2"], params["3"], params["4"]].map(&:to_i)
  session[:game].history << session[:game].guess
  session[:game].peg_history << session[:game].board.organize_pegs(session[:game].guess.join("").to_i)
  
  session[:game].turn_decrease
    
  erb :index, :locals => { 
    :turn        => session[:game].turn,
    :guess       => session[:game].guess,
    :game_over   => session[:game].game_over?,
    :victory     => session[:game].victory?,
    :history     => session[:game].history,
    :peg_history => session[:game].peg_history,
    :answer      => session[:game].answer,
    :end_message => session[:game].message
  }
end

def to_color(num)
  hash = {
    1 => :red,
    2 => :green,
    3 => :yellow,
    4 => :blue,
    5 => :purple,
    6 => :cyan
  }
  return hash[num].to_s
end

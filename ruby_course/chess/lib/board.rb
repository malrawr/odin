class Board
  attr_accessor :board, :board_multi
  def initialize
    @board = []
    (0..7).each { |x| (0..7).each { |y| @board << Node.new(x, y) } }
    @board_multi = @board.each_slice(8).to_a
    set_board
    #move_chessman([1,0], [2,0], :white)
    #move_chessman([2,0], [3,1], :white)
    #move_chessman([1,1], [3,1], :white)
    #move_chessman([6,2], [4,2], :black)
    #draw_board
  end 
  
  def existing_coordinate?(coordinate)
    valid = []
    (0..7).each { |x| (0..7).each { |y| valid << [x, y] } }
    valid.find_index(coordinate) ? true : false
  end
  
  def set_board
    #(8..15).each { |i| @board[i] = Node.new(@board[i].x, @board[i].y, Pawn.new(:white, true)) }
    @board[0] = Node.new(@board[0].x, @board[0].y, Rook.new(:white, true))
    @board[1] = Node.new(@board[1].x, @board[1].y, Knight.new(:white, true))
    @board[2] = Node.new(@board[2].x, @board[2].y, Bishop.new(:white, true))
    @board[3] = Node.new(@board[3].x, @board[3].y, Queen.new(:white, true))
    @board[4] = Node.new(@board[4].x, @board[4].y, King.new(:white, true))
    @board[5] = Node.new(@board[5].x, @board[5].y, Bishop.new(:white, true))
    @board[6] = Node.new(@board[6].x, @board[6].y, Knight.new(:white, true))
    @board[7] = Node.new(@board[7].x, @board[7].y, Rook.new(:white, true))
    
    #(48..55).each { |i| @board[i] = Node.new(@board[i].x, @board[i].y, Pawn.new(:black, true)) }
    @board[56] = Node.new(@board[56].x, @board[56].y, Rook.new(:black, true))
    @board[57] = Node.new(@board[57].x, @board[57].y, Knight.new(:black, true))
    @board[58] = Node.new(@board[58].x, @board[58].y, Bishop.new(:black, true))
    @board[59] = Node.new(@board[59].x, @board[59].y, Queen.new(:black, true))
    @board[60] = Node.new(@board[60].x, @board[60].y, King.new(:black, true))
    @board[61] = Node.new(@board[61].x, @board[61].y, Bishop.new(:black, true))
    @board[62] = Node.new(@board[62].x, @board[62].y, Knight.new(:black, true))
    @board[63] = Node.new(@board[63].x, @board[63].y, Rook.new(:black, true))
    @board_multi = @board.each_slice(8).to_a
  end
  
  def check_king?(color)
    movements = anti_king_movements(color)
    @board.each do |node|
      if node.chessman.is_a?(King) && node.chessman.color == color
        if movements.include?(node.coordinate)
          return true
        end
      end
    end
    return false
  end
  
  def move(start, target, color)
    start_node = @board_multi[start[0]][start[1]]
    target_node = @board_multi[target[0]][target[1]]
    
    if check_king?(color)
      puts "You are in check, you must move the king!"
      return nil
    end
    
    if start_node.chessman.is_a?(Empty)
      puts "You selected an empty space!"
      return nil
    end
    
    if start_node.chessman.color != color
      puts "That piece is not yours!" 
      return nil
    end
    
    if start_node.chessman.is_a?(King)
      if anti_king_movements(color).include?(target_node.coordinate)
        puts "You can't move the King here! You'll be in check!"
        return nil
      end
    end
    if start_node.chessman.is_a?(Pawn)
      move_pawn(start_node, target_node, color)
      puts "Moving Pawn"
      p start
    else
      move_chessman(start_node, target_node, color)
      puts "Moving Rook"
      p start
    end
  end
  
  def define_object(object, color)
    object.class.new(color)
  end
  
  def move_chessman(start_node, target_node, color)
    object = define_object(start_node.chessman, color)
    if check_path(start_node.chessman, target_node.coordinate)
      if start_node.chessman.color != target_node.chessman.color
        @board_multi[target_node.x][target_node.y] = Node.new(target_node.x, target_node.y, object)
        @board_multi[start_node.x][start_node.y] = Node.new(start_node.x, start_node.y)
      else
        puts "You cannot capture your own piece."
        return nil
      end
    else
      puts "That is not a valid point for this unit."
      return nil
    end
  end
  
  def move_pawn(start_node, target_node, color)
    object = define_object(start_node.chessman, color)
    if check_path(start_node.chessman, target_node.coordinate)
      @board_multi[target_node.x][target_node.y] = Node.new(target_node.x, target_node.y, object)
      @board_multi[start_node.x][start_node.y] = Node.new(start_node.x, start_node.y)
    elsif pawn_captures(target_node.chessman).include?(target_node.coordinate)
      if target_node.chessman.is_a?(Empty)
        puts "You can't capture an empty spot"
        return nil
      end
      if start_node.chessman.color != target_node.chessman.color 
        @board_multi[target_node.x][target_node.y] = Node.new(target_node.x, target_node.y, object)
        @board_multi[start_node.x][start_node.y] = Node.new(start_node.x, start_node.y)
      else
        puts "You cannot capture your own piece."
        return nil
      end
    else
      puts "That is not a valid point for this unit."
      return nil
    end
  end

  def check_path(chessman, target)
    #return false if !valid_movements(chessman).include?(target)
    p valid_movements(chessman)
    p target
    valid_movements(chessman).each do |move_array|
      if move_array.include?(target)
        puts "The target is inside this array!"
        move_array.each do |move|
          x, y = move[0], move[1]
          if move == target
            puts "Yeah we can move here"
            return true
          end
          if !@board_multi[x][y].chessman.is_a?(Empty)
            puts "There's another piece in the way: #{@board_multi[x][y].chessman.inspect}"
            return false
          end
        end
      end
   end
   puts "Nothing happened"
   return nil
  end
  
  def valid_movements(chessman)
    chessman.movements.each do |movements|
      movements.select! { |move| existing_coordinate?(move) }
    end
  end
  
  def pawn_captures(chessman)
    tmp = []
    possible_captures = chessman.captures
    possible_captures.each do |move| 
      tmp << move if existing_coordinate?(move) 
    end
    return tmp
  end
  
  def move_king(start_node, target_node, color)
    object = define_object(start_node.chessman, color)
    if check_path(start_node.chessman, target_node.coordinate)
      if start_node.chessman.color != target_node.chessman.color
        @board_multi[target_node.x][target_node.y] = Node.new(target_node.x, target_node.y, object)
        @board_multi[start_node.x][start_node.y] = Node.new(start_node.x, start_node.y)
      else
        puts "You cannot capture your own piece."
        return nil
      end
    else
      puts "That is not a valid point for this unit."
      return nil
    end
  end
  
  def anti_king_movements(color)
    case color
    when :white
      color = :black
    when :black
      color = :white
    end
    active_node = @board.select! { |node| node.chessman.color == color }
    if active_node.nil?
      puts "The active nodes are nil for some reason"
      return nil
    end
    enemy_active = []
    active_node.each do |node|
      if !node.chessman.is_a?(Knight) && !node.chessman.is_a?(Pawn)
        enemy_active << valid_movements(node.chessman) 
      end
    end
    tmp = []
    enemy_active.flatten(1).uniq.each do |array_move|
      array_move.each do |move|
        @board_multi[move[0]][move[1]].chessman.is_a?(Empty) ? tmp << move : break
      end
    end
    active_node.each do |node|
      if node.chessman.is_a?(Knight) 
        valid_movements(node.chessman).flatten(1).each { |move| tmp << move }
      elsif node.chessman.is_a?(Pawn)
        tmp << pawn_captures(node.chessman)
      end
    end
    return tmp
  end
  
  def draw_board
    board = @board_multi.flatten
    str = ""
    #str << "    𝐀 𝐁 𝐂 𝐃 𝐄 𝐅 𝐆 𝐇\n"
    str << "  Ⓐ Ⓑ Ⓒ Ⓓ Ⓔ Ⓕ Ⓖ Ⓗ\n"
    str << "╚\u202F════════════════════\u202F╝\n"
    (0..63).to_a.each do |i|
      node = board[i]
      str << "║\u202F" if i % 8 == 0
      str << "\u202F"
      str << node.chessman.symbol
      str << "\u202F ║\n" if i % 8 == 7
    end
    str << "╔\u202F════════════════════\u202F╗\n"
    str = str.split("\n").each.with_index do |string, str_index|
      case str_index
      when 0
        string.prepend("  ") 
      when 1
        string.prepend("⬤\u202F")
      when 2
        string.prepend("⓵\u202F")
      when 3
        string.prepend("⓶\u202F")
      when 4
        string.prepend("⓷\u202F")
      when 5
        string.prepend("⓸\u202F")
      when 6
        string.prepend("⓹\u202F")
      when 7
        string.prepend("⓺\u202F")
      when 8
        string.prepend("⓻\u202F")
      when 9
        string.prepend("⓼\u202F")
      when 10
        string.prepend("⬤\u202F")
      end
    end.join("\n")
    puts str.split("\n").reverse
  end
end
#⛝  ⛆   ⛬  ⛼  ⛚
#♙♘♗♖♕♔♚♛♜♝♞♟
=begin
8 ║♜ ♞ ♝ ♛ ♚ ♝ ♞ ♜
7 ║♟ ♟ ♟ ♟ ♟ ♟ ♟ ♟
6 ║… … … … … … … … … … … …
5 ║… … … … … … … … … …
4 ║… … … … … … … …
3 ║… … ♘ … … … … …
2 ║♙ ♙ ♙ ♙ ♙ ♙ ♙ ♙
1 ║♖ … ♗ ♕ ♔ ♗ ♘ ♖
— ╚═══════════════
——a   b   c   d   e   f   g   h
=end

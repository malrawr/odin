class Queen < Chessman
  attr_accessor :color, :start_position
  def initialize(color, start_position = false)
    super(color)
    @start_position = start_position
    case @color
    when :white
      @symbol = "\u2655" #♕
    when :black
      @symbol = "\u265B" #♛
    end
  end
  
  def movements
    array = []
    (1..7).each { |i| array << [@x + i, @y] }
    (1..7).each { |i| array << [@x - i, @y] }
    (1..7).each { |i| array << [@x, @y + i] }
    (1..7).each { |i| array << [@x, @y - i] }
    (1..7).each { |i| array << [@x + i, @y + i] }
    (1..7).each { |i| array << [@x + i, @y - i] }
    (1..7).each { |i| array << [@x - i, @y + i] }
    (1..7).each { |i| array << [@x - i, @y - i] }
    return array.each_slice(7).to_a
  end
end
class King < Chessman
  attr_accessor :color, :start_position
  def initialize(color, start_position = false)
    super(color)
    @start_position = start_position
    case @color
    when :white
      @symbol = "\u2654" #♔
    when :black
      @symbol = "\u265A" #♚
    end
  end
  
  def movements
    array = []
    (1..1).each { |i| array << [@x + i, @y] }
    (1..1).each { |i| array << [@x - i, @y] }
    (1..1).each { |i| array << [@x, @y + i] }
    (1..1).each { |i| array << [@x, @y - i] }
    (1..1).each { |i| array << [@x + i, @y + i] }
    (1..1).each { |i| array << [@x + i, @y - i] }
    (1..1).each { |i| array << [@x - i, @y + i] }
    (1..1).each { |i| array << [@x - i, @y - i] }
    return array.each_slice(1).to_a
  end
end
class Pawn < Chessman
  attr_accessor :color, :start_position
  def initialize(color, start_position = false)
    super(color)
    @start_position = start_position
    case @color
    when :white
      @symbol = "\u2659" #♙
    when :black
      @symbol = "\u265F" #♟
    end
  end
  
  def movements
    array = []
    case @color
    when :white
      array << [@x + 1, @y]
      array << [@x + 2, @y] if @start_position
    when :black
      array << [@x - 1, @y]
      array << [@x - 2, @y] if @start_position
    end
    return array.sort.each_slice(2).to_a if @start_position
    return array.sort.each_slice(1).to_a
  end
  
  def captures
    array = []
    case @color
    when :white
      array << [@x + 1, @y + 1]
      array << [@x + 1, @y - 1]
    when :black
      array << [@x - 1, @y + 1]
      array << [@x - 1, @y - 1]
    end
    return array
  end
  
  def en_passant
  end
end
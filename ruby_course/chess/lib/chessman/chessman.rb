class Chessman
  attr_accessor :color, :position, :symbol, :x, :y
  def initialize(color, position = nil)
    @color = color
    @position = position
  end 
end

=begin
  def chess_symbol(color, symbol)
    chess_symbol = {
      :white => {
        :king   => "♔",
        :queen  => "♕",
        :rook   => "♖",
        :bishop => "♗",
        :knight => "♘",
        :pawn   => "♙"
      },
      :black => {
        :king   => "♚",
        :queen  => "♛",
        :rook   => "♜",
        :bishop => "♝",
        :knight => "♞",
        :pawn   => "♟"
      }
    }
    return chess_symbol[color][symbol]
  end
=end
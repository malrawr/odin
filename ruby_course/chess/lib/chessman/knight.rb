class Knight < Chessman
  attr_accessor :color, :start_position
  def initialize(color, start_position = false)
    super(color)
    @start_position = start_position
    case @color
    when :white
      @symbol = "\u2658" #♘
    when :black
      @symbol = "\u265E" #♞
    end
  end
  
  def movements
    array = [[@x + 1, @y + 2],[@x + 2, @y + 1],[@x + 2, @y - 1],[@x + 1, @y - 2],
             [@x - 1, @y + 2],[@x - 2, @y + 1],[@x - 2, @y - 1],[@x - 1, @y - 2]]
    return array.sort.each_slice(1).to_a
  end
end

class Node
    attr_accessor :x, :y,:coordinate, :chessman
    def initialize(x , y, chessman = Empty.new)
      @x = x
      @y = y
      @coordinate = [@x, @y]
      @chessman = chessman
      @chessman.position = @coordinate
      @chessman.x = @x
      @chessman.y = @y
    end
end

class Empty
  attr_accessor :symbol, :x, :y, :position, :color
  def initialize
    @symbol = "\u26C6" # ⛆
  end
end
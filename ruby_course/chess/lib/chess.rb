module Chess
  require_relative 'board'
  require_relative 'player'
  require_relative 'node'
  require_relative 'chessman/chessman'
  require_relative 'chessman/pawn'
  require_relative 'chessman/knight'
  require_relative 'chessman/rook'
  require_relative 'chessman/bishop'
  require_relative 'chessman/queen'
  require_relative 'chessman/king'
  
  class Game
    attr_accessor :board
    def initialize
     @board = Board.new
     game_loop
    end
      
    def game_loop
      players = [Player.new("Player 1", :white), 
                 Player.new("Player 2", :black)]
      
      99.times do
        players.each do |player|
          @board.draw_board
          puts "#{player.name}: Select a piece."
          start = input_translate(gets.chomp.split(''))
          puts "#{player.name}: Select a target."
          target = input_translate(gets.chomp.split(''))
          
          @board.move(start, target, player.color)
        end
      end
    end
    
    def input_translate(array)
      array[0] = array[0].ord - 97 
      array[1] = array[1].to_i - 1
      return array.reverse
    end
    
    def verify_input
    end
  end
  
end
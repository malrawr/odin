class Game
  attr_accessor :board
  def initialize
   @board = Board.new
   game_loop
  end
    
  def game_loop
    players = [Player.new("Player 1", :white), 
               Player.new("Player 2", :black)]
    @board.draw_board
    99.times do
      players.each do |player|
        puts "#{player.name}: Select a piece."
        start = input_translate(gets.chomp.split(''))
        puts "#{player.name}: Select a target."
        target = input_translate(gets.chomp.split(''))
        
        move_chessman(start, target, player.color)
        @board.draw_board
      end
    end
  end
  
  def input_translate(array)
    array[0] = array[0].ord - 97 
    array[1] = array[1] - 1
    return array
  end
  
  def verify_input
  end
end
class String
  def colorize(color_code)
    "\e[#{color_code}m#{self}\e[0m"      
  end
  
  def red;    colorize(31) end
  def green;  colorize(32) end
  def yellow; colorize(33) end
  def blue;   colorize(34) end
  def purple; colorize(35) end
  def cyan;   colorize(36) end
end

module Mastermind
  class Board
    attr_accessor :pairs, :code
    def initialize
      @pairs = {
        1 => "⬤".red,
        2 => "⬤".green,
        3 => "⬤".yellow,
        4 => "⬤".blue,
        5 => "⬤".purple,
        6 => "⬤".cyan
      }

      @code = generate_code
    end
    
    def display_code
      @code.join('').to_i
    end
    
    def generate_code
      [rand(1..6),rand(1..6),rand(1..6),rand(1..6)].map(&:to_s)
    end
   
    def display(number)
      split_number(number).each.with_index do |x, i|
        print "#{@pairs[x.to_i]} "
        if i == 1
          organize_pegs(number).each { |x| print x }
          print " "
        end
      end
      puts ""
    end
    
    def organize_pegs(number)
      peg_array = []
      if include_number?(number)
        peg_array += write_closed_pegs if matching_index?(number)
        peg_array += write_open_pegs(number)
      end
      return peg_array
    end
    
    def write_open_pegs(number)
      open_pegs = []
      ((@code & split_number(number)).length - write_closed_pegs.length).times do |x|
        open_pegs << "◎"
      end
      return open_pegs
    end
   
    def include_number?(number)
      unless (@code & split_number(number)).empty?
        return true
      end
      return false
    end
    
    def share_index?(element, index)
      @code.index(element) == index
    end

    def matching_index?(number)
      @tmp = []
      split_number(number).each.with_index do |x, i|
        if @code[i].include?(x)
          @tmp << x
        else
          @tmp << nil
        end
      end
      #p @tmp
      if @tmp.compact.empty?
        return false
      end
      return true
    end
    
    def write_closed_pegs
      closed_pegs = []
      @tmp.each.with_index do |element, index| 
        closed_pegs << "◉" if share_index?(element, index)
      end
      return closed_pegs
    end

    
    def split_number(number)
      number.to_s.split('')
    end
    
  end
  class Player 
    class Codebreaker; end
    class Codemaker; end
  end
  class Game
    def initialize
      hello
      @board = Board.new
      game_loop
    end  
    
    def hello
      puts "Do you want to be the Codebreaker or the Codemaker? (1,2)"
      input = gets.chomp.to_i
      if input == 1
        puts "Selected Codebreaker"
      elsif input == 2
        puts "Selected Codemaker"
      end
      puts "MASTERMIND... you have 12 chances to break the code"
    end
    
    def game_loop
      (1..12).each do |x|
        puts "Try: #{x}"
        input = prompt_color_code
        @board.display(input)
        victory if input == @board.display_code
      end
      puts "You have lost! The number was #{@board.display_code}"
    end
    
    def victory
      puts "Victory!"
      exit
    end
    
    def prompt_color_code
      code = ""
      until valid_code?(code)
        code = gets.chomp.to_i
        puts "Invalid code, try again." unless valid_code?(code)
      end
      code
    end
    
    def valid_code?(code)
      code = code.to_s.split('').map(&:to_i)
      unless code.length == 4 && code.max < 7 && code.min > 0 
        return false
      else
        return true
      end
    end
    
  end
end

Mastermind::Game.new

require 'irb'
IRB.start(__FILE__)


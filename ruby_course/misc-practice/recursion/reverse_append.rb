def reverse_append(arr, n)
  return arr << 0 if n == 0
  arr.unshift(n)
  append(arr, n-1)
end

reverse_append([], 2) # => [0, 1, 2]
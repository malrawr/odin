def append(arr, n)
  return arr << 0 if n == 0
  arr << n
  append(arr, n-1)
end

append([], 2) # => [2, 1, 0]
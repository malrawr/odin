def bottles(n)
  (puts "No more bottles of beer on the wall"; return )if n == 0
  puts "#{n} bottles of beer on the wall."
  bottles(n-1)
end

bottles(5)
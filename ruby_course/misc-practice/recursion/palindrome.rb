def palindrome?(str)
  return true if str.length == 1 || str.length == 0
  str[0] == str[-1] ? palindrome?(str[1..-2]) : false
end

puts palindrome?("madam") # => true
puts palindrome?("string") # => false
def merge_sort(a)
  n = a.length
  return a if n < 2
  left = a[0...n/2]
  right = a[n/2...n]
  left = merge_sort(left)
  right = merge_sort(right)
  return merge(left, right)
end

def merge(a, b, c = [])
  while a.any? && b.any?
    if a[0] > b[0]
      c.push(b[0])
      b.delete_at(0)
    else
      c.push(a[0])
      a.delete_at(0)
    end
  end
  while a.any?
    c.push(a[0])
    a.delete_at(0)
  end
  while b.any?
    c.push(b[0])
    b.delete_at(0)
  end
  return c
end

puts merge_sort([1,45,23,8,2,46,5])
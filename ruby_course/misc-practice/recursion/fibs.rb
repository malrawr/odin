def fibs(n)
  n == 0 || n == 1 ? n : fibs(n-1) + fibs(n-2)
end

def fibs_rec(n)
  n.next.times.map(&method(:fibs))
end

p fibs_rec(5)
require "cipher"

describe "#cipher_shift" do
  
  context "given one argument" do
    it "raises a 'ArgumentError' exception" do
      expect { cipher_shift("") }.to raise_error ArgumentError
    end
  end
  
  context "given two arguments"
  
    context "given 'string', '0'" do 
      it "returns 'string'" do
        expect(cipher_shift("string", 0)).to eql("string")
      end
    end
  
    context "given 'string', '5'" do
      it "returns 'xywnsl'" do
        expect(cipher_shift("string", 5)).to eql("xywnsl")
      end
    end
    
    context "given 'string', '10'" do
      it "returns 'cdbsxq'" do
        expect(cipher_shift("string", 10)).to eql("cdbsxq")
      end
    end
    
    context "given 'string', '31'" do
      it "returns output of 'cipher_shift('string', 5)'" do
        expect(cipher_shift("string", 31)).to eql(cipher_shift("string", 5))
      end
    end
    
    context "given 'STRING', 5" do
      it "returns 'XYWNSL'" do
        expect(cipher_shift("STRING", 5)).to eql("XYWNSL")
      end
    end
    
    context "given 'sTRiNg', 5" do
      it "returns 'xYWnSl'" do
        expect(cipher_shift("sTRiNg", 5)).to eql("xYWnSl")
      end
    end
    
    context "given '#^&$', '9'" do
      it "returns '#^&$'" do
        expect(cipher_shift("#^&$", 9)).to eql("#^&$")
      end
    end
    
    context "given ' ', 9" do
      it "returns ' '" do
        expect(cipher_shift(' ', 9)).to eql(' ')
      end
    end
    
    context "given 'What a string!', '5'" do
      it "returns 'Bmfy f xywnsl!'" do
        expect(cipher_shift("What a string!", 5)).to eql("Bmfy f xywnsl!")
      end
    end
  
end
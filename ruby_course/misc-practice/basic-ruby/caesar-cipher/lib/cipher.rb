def cipher_shift(input, shift)
    alphabet = ('a'..'z').to_a
    output = ""
    
    input.each_char do |c| 
        if alphabet.index(c.downcase) == nil
            output << c
        elsif c == c.upcase
            output << calc_shift(c, shift).upcase
        else 
            output << calc_shift(c, shift)
        end
    end
    return output
end

def calc_shift(character, shift)
    alphabet = ('a'..'z').to_a
    character.downcase!

    if alphabet.index(character) + shift > 26
        return alphabet[(alphabet.index(character) + shift) % 26].to_s
    else
        return alphabet[alphabet.index(character) + shift].to_s
    end
end

#puts cipher_shift("What a string!", 5)
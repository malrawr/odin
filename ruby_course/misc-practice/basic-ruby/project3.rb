=begin
Implement a method #substrings that takes a word as the first argument and then an 
array of valid substrings (your dictionary) as the second argument. It should return a hash 
listing each substring (case insensitive) that was found in the original string and how many 
times it was found.
=end

dictionary = ["below","down","go","going","horn","how","howdy","it","i","low","own","part","partner","sit"]

def substrings(string, dictionary)

    hash_listing = {}

    string.downcase.split(" ").map do |word|
        dictionary.each_with_index do |entry, index|
            if word.include?(dictionary[index]) 
                if hash_listing.has_key?(entry)
                    hash_listing[entry] = hash_listing[entry].next
                else
                    hash_listing[entry] = 1 
                end
            end
        end
    end

    puts hash_listing

end

substrings("Howdy partner, sit down! How's it going?", dictionary)


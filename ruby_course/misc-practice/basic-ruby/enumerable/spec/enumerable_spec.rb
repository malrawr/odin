require 'enumerable'

describe Enumerable do
  
  let(:enum) { (1..3) }
  let(:arr_blank) { [] }
  let(:arr) { [1,2,3,4] }
  
  
  describe "#my_each" do
    
    context "given an empty block" do
      it "returns Enumerator" do
        expect(arr.my_each).to be_is_a(Enumerator)
      end
    end
    
    context "given a block" do
      it "returns each element in self" do
        expect { |block| arr.my_each(&block) }.to yield_successive_args(1,2,3,4)
      end
    end
    
  end
  
  describe "#my_each_with_index" do

    context "given an empty block" do
      it "returns Enumerator" do
        expect(arr.my_each_with_index).to be_is_a(Enumerator)
      end
    end
    
    context "given a block" do
      it "returns both index and elements" do
        answer = []
        [1,2,3,4].my_each_with_index { |n, i| answer << i}
        expect(answer).to eql([0,1,2,3])
      end
    end
  end
  
  describe "#my_select" do
    
    context "given an empty block" do
      it "returns Enumerator" do
        expect(arr.my_select).to be_is_a(Enumerator)
      end
    end
    
    context "given a block" do
      it "returns elements that resolve to true" do
        answer = [1,2,3,4].my_select { |x| x.even? }
        expect(answer).to eql([2,4])
      end
    end
  end
  
  describe "#my_all?" do
    context "given an empty block" do
      it "returns Enumerator" do
        expect(arr.my_select).to be_is_a(Enumerator)
      end
    end
    
    context "given a an array" do
      it "returns true if there are no false or nil elements" do
        expect([].my_all?).to eql(true)
        expect([false,1].my_all?).to eql(false)
      end
    end
  end
  
  describe "#my_any" do
    context "given an empty block" do
      it "returns Enumerator" do
        expect(arr.my_select).to be_is_a(Enumerator)
      end
    end
    
    context "given a block" do
      it "returns true if no falsey values" do
        expect([1,2,3,4].my_any? { |num| num <= 4}).to eql(true)
        expect([1,2,3].my_any? { |num| num < 4}).to eql(true)
      end
    end
    
    context "given an array" do
      it "returns true if there are no false or nil elements" do
        expect([2,4].my_all?).to eql(true)
        expect([].my_all?).to eql(true)
      end
    end
  end
    
    
end
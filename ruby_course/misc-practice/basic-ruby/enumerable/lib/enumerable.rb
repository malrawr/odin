module Enumerable
  def my_each
    return self.to_enum unless block_given? 
    #return self if self.is_a?(Enumerable)
    i = 0
    while i < self.size
      yield(self[i])
        i += 1
    end      
    self
  end

  def my_each_with_index
    return self.to_enum unless block_given?
    i = 0
      while i < self.size
        yield(self[i], i)
        i += 1
      end
    self
  end

    def my_select
      return self.to_enum unless block_given?
      tmp = []
      self.my_each do |x| 
        if yield(x) then tmp << x end
      end
      tmp
    end

    def my_all?
      if block_given?
        self.my_each { |x| return false unless yield(x) }
        return true
      end
      self.my_each { |x| return false unless x}
      return true
    end

    def my_any?
      if block_given?
        self.my_each { |x| return true if yield(x) }
        return false
      end
      self.my_each { |x| return true if x } unless self.size == 0
      return false
    end

    def my_none?
        self.my_each do |x|
            if block_given?
                if yield(x) then return false end
            else
                return true
            end
        end
        return true
    end

    def my_count_orig(num = nil)
        if num 
            counter = 0
            num, counter = counter, num
            self.my_each do |x|
                if counter == x
                    num += 1
                end
            end
        elsif block_given?
            num = 0
            self.my_each do |x|   
                if yield(x)
                    num += 1
                end
            end
        elsif num == nil
            num = self.length
        else
            num = 0
        end
        return num
    end

    def my_count(num = nil)
        if num || block_given?
            counter = 0
            num, counter = counter, num
            self.my_each do |x|
                if block_given?
                    if yield(x) then num +=1 end
                end
                if counter == x then num += 1 end
            end
        elsif num.nil?
            num = self.length
        else
            num = 0
        end
        return num
    end

    def my_map
        i = 0
        out = []
        tmp = self.to_a
        while i <  tmp.size
            out << yield(tmp[i])
            i += 1
        end
        out
    end

    def my_inject(num = nil)
        if num.nil? then num = 0 end
        self.my_each { |x| num = yield(num, x) }
        return num
    end

    def my_inject_old(num = nil)
        result = 0
        tmp = self.to_a
        if num.nil? 
            #num = tmp[0]
            num.to_i 
            num = 0
            i = tmp.length
            while i >= 0
                num = yield(num, tmp[i])
                i += 1
            end
        else
            self.my_each { |x| num = yield(num, x) }
        end
        return num
    end

    def my_map_proc(&block)
        i = 0
        out = []
        tmp = self.to_a
        while i < tmp.length
            out << yield(tmp[i])
            i += 1
        end
        out
    end
end

def multiply_els(array)
    array.my_inject(1) { |sum, n| sum * n }
end

#p array.my_each { |x| x + 1 }
#array.my_each_with_index { |x, y| puts "Element: #{x} and Index: #{y}" }
#p array.my_select { |x| x % 2 == 0 }
#p array.my_all?
#p array.my_all? { |x| x % 2 == 0}
#p array.my_any? { |x| x % 8 == 1}
#p array.my_any?
#p array.my_none?
#p array.my_none? { |x| x == 8}
#p array.my_count_failed(10)
#p array.my_count_failed(2)
#p array.my_count_failed { |x| x%2 == 0 }
#p array.my_map { |i| i*i }  
#p (1..4).my_map { |i| i*i }
#p (5..10).my_inject { |sum, n| sum + n }
#p (5..10).my_inject(2) { |product, n| product + n }
#p (5..10).my_inject(1) { |product, n| product + n }
#p (5..10).my_inject { |product, n| product + n }
#p (5..10).my_inject { |product, n| product  n }
#p (5..10).my_inject(:+)
#p multiply_els([2,4,5])
#p [2,4,5].my_inject(:*)
#double = Proc.new {|num| num * 2}

#p [2,4,6].my_map_proc(&double)
def stock_picker(stocks) 

    limit = 0
    limit_array = []

    stocks.each do |price|  
        (stocks.index(price)...stocks.length).each do |day_index|
            if stocks[day_index] - price >= limit
                limit = stocks[day_index] - price
                limit_array = [stocks.index(price), day_index]
            end
        end
    end
    return limit_array
end

p stock_picker([17,3,6,9,15,8,6,1,10])
class Node
  attr_accessor :value, :left, :right
  def initialize(value = nil)
    if value.is_a?(Array)
      @value = value.shift
      self.build_tree(value)
    else
      @value = value
    end
  end
  
  def insert(value) 
    case value <=> self.value
    when -1
      insert_left(value)
    when 1
      insert_right(value)
    when 0
      nil
    end
  end
    
  def insert_left(value)
    if self.left.nil?
      self.left = Node.new(value)
    else
      self.left.insert(value)
    end
  end

  def insert_right(value)
    if self.right.nil?
      self.right = Node.new(value)
    else
      self.right.insert(value)
    end
  end
  
  def build_tree(array)
    array.each do |element|
      self.insert(element)
    end
  end
  
  def bfs(value) # breadth first search
    tmp = [] << self
    while tmp.any?
      node = tmp.shift
      return node.value if node.value == value
      
      tmp << node.left unless node.left.nil?
      tmp << node.right unless node.right.nil?
    end
    return nil
  end
  
  def dfs(value) # depth first search
    tmp = [] << self
    while tmp.any?
      node = tmp.pop
      return node.value if node.value == value
      
      tmp << node.left unless node.left.nil?
      tmp << node.right unless node.right.nil?
      
      return nil if tmp.last == self
    end
  end
  
  def dfs_rec(value) # depth first search recursive
    return self.value if self.value == value
    self.left.dfs_rec(value) unless self.left.nil?
    self.right.dfs_rec(value) unless self.right.nil?
  end
  
  def display
    "#{self.value}:(#{self.left.display})|(#{self.right.display})"
  end
end

one = Node.new([1,2,3,4,5,6,7,8,9,10])
two = Node.new([1, 7, 4, 23, 8, 9, 4, 3, 5, 7, 9, 67, 6345, 324])
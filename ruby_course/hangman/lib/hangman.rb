module Hangman
  require 'yaml'
  class Dictionary
    def initialize(min, max)
      @dictionary = File.readlines("5desk.txt")
      @dictionary = @dictionary.map(&:chomp).map(&:downcase)
      @dictionary = select_length(min, max)
    end
    
    def select_length(min, max)
      @dictionary.select do |word|
        word if word.length >= min && word.length <= max
      end
    end
    
    def random_word
      @dictionary.sample
    end
    
  end
  
  class Game
    attr_reader :word, :word_array, :guess_array
    def initialize
      @word = Dictionary.new(5, 12).random_word
      @word_array = @word.split('')
      @guess_array = @word_array.map { |x| "_" }
      @turn = 1
    end
    
    def guess(letter)
      @word_array.each_index do |index|
        if @word_array[index] == letter
          @guess_array[index] = letter
        end
      end
    end
    
    def victory?
      @guess_array.include?('_') ? false : true
    end
    
    def game_loop
      while @turn <= 12
        puts "Turn #{@turn}: Guess the word! Type 'exit' if you want to save and continue later"
        answer = gets.chomp
        answer == "exit" ? save_game : guess(answer)
        p @guess_array
        finish_game if victory? 
        @turn += 1
      end
      puts "You lost! The word was #{@word}!"
    end 
    
    def finish_game
      puts "You won!"
      exit
    end
    
    def to_json
      JSON.dump ({
        :word        => @word,
        :word_array  => @word_array,
        :guess_array => @guess_array
      })
    end
  
    def save_game
      save_file = YAML.dump(self)
      #puts save_file
      SaveFile.new.write(save_file)
      exit
    end
    
  end

  class SaveFile
    def write(json)
      Dir.mkdir('save') unless Dir.exists?('save')
      timestamp = Time.new.strftime("%s")
      filename = "save/#{timestamp}"
      
      File.open(filename, 'w') do |file|
        file.puts json 
      end
    end
    
    def read(timestamp)
      filename = "save/#{timestamp}"
      File.open(filename, 'r') do |file|
        return YAML.load(file)
      end
    end
    
    def list_files
      Dir.entries('save').select { |f| !f.include?('.') }
    end
    
    def empty?
      Dir.entries('save').size == 2
    end
    
  end
  
end


puts "Hangman is starting!"
puts "Do you want to load an external file?"
answer = gets.chomp.downcase
if answer == "yes" && !Hangman::SaveFile.new.empty?
  p Hangman::SaveFile.new.list_files
  puts "Which do you want to load?"
  file = gets.chomp.downcase
  game = Hangman::SaveFile.new.read(file)
else
  game = Hangman::Game.new
end

game.game_loop
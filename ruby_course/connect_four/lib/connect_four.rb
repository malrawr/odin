class String
  def colorize(color_code); "\e[#{color_code}m#{self}\e[0m" end
  def red;    colorize(31) end
  def yellow; colorize(33) end
end

module ConnectFour
  class Node
    attr_accessor :x, :y,:coordinate, :circle_node 
    def initialize(x , y)
      @x = x
      @y = y
      @coordinate = [@x, @y]
      @circle_node = Circle.new(:white, nil)
    end
  end
  
  class Circle
    attr_accessor :color, :symbol, :active
    def initialize(color, active = true)
      @active = active
      @color = color
      case @color
      when :white   then @symbol = "⬤"
      when :red     then @symbol = "⬤".red
      when :yellow  then @symbol = "⬤".yellow
      end
    end
  end
  
  class Player
    attr_accessor :name, :circle
    def initialize(name, circle)
      @name = name
      @circle = circle
    end
  end
  
  class Game
    attr_accessor :board
    def initialize
      @board = Board.new
      game_loop
    end
    
    def game_loop
      players = [Player.new("Player 1", Circle.new(:red)), 
                 Player.new("Player 2", Circle.new(:yellow))]
      @board.draw_board
      43.times do
        players.each do |player|
          puts "#{player.name}: Choose an index? 1 - 7"
          index = verify_input
          @board.add_circle(index, player.circle)
          @board.draw_board
          if @board.victory?(player.circle.color)   
            puts "#{player.name}: You win!"
            exit
          end
        end
      end
      puts "It's a tie! All spots are filled."
    end
    
    def verify_input
      input = 0
      until input >= 1 && input <= 7 && @board.index_exists?(input - 1)
        input = gets.chomp.to_i
        puts "That index is full!" unless @board.index_exists?(input - 1)
      end
      return input - 1
    end
  end
  
  class Board
    attr_accessor :board
    def initialize
      @board = []
      (0..5).each do |x|
        (0..6).each do |y|
          @board << Node.new(x, y)
        end
      end
    end
    
    def draw_board
      str = ""
      str << " ⓵ ⓶ ⓷ ⓸ ⓹ ⓺ ⓻ \n"
      (0..41).to_a.each do |i|
        node = @board[i]
        str << "|"
        str << node.circle_node.symbol
        str << "|\n" if i % 7 == 6
      end
      puts str.split("\n").reverse
    end
    
    def index_exists?(index)
      return false if @board[index].nil?
      node = @board[index]
      if node.circle_node.active.nil?
        return true
      else
        index_exists?(index + 7)
      end
    end
    
    def add_circle(index, obj)
      return nil if @board[index].nil?
      node = @board[index]
      if node.circle_node.active.nil?
        node.circle_node = obj
      else
        add_circle(index + 7, obj)
      end
    end
    
    def victory?(color); check_diagonal?(color) || check_consecutive?(color) end
    
    def check_diagonal?(color)
      tmp = []
      tmp << (0..3).collect { |i| @board.each_slice(7).to_a[i+2][i] }
      tmp << (0..4).collect { |i| @board.each_slice(7).to_a[i+1][i] }
      tmp << (0..5).collect { |i| @board.each_slice(7).to_a[i][i] }
      tmp << (1..6).collect { |i| @board.each_slice(7).to_a[i-1][i] }
      tmp << (2..6).collect { |i| @board.each_slice(7).to_a[i-2][i] }
      tmp << (3..6).collect { |i| @board.each_slice(7).to_a[i-3][i] }
      
      tmp << (0..3).collect { |i| @board.each_slice(7).to_a.reverse[i+2][i] }
      tmp << (0..4).collect { |i| @board.each_slice(7).to_a.reverse[i+1][i] }
      tmp << (0..5).collect { |i| @board.each_slice(7).to_a.reverse[i][i] }
      tmp << (1..6).collect { |i| @board.each_slice(7).to_a.reverse[i-1][i] }
      tmp << (2..6).collect { |i| @board.each_slice(7).to_a.reverse[i-2][i] }
      tmp << (3..6).collect { |i| @board.each_slice(7).to_a.reverse[i-3][i] }
      return check_array?(tmp, color)
    end
    
    def check_consecutive?(color)
      tmp = []
      (0..6).each do |i|
        tmp << (0..5).collect { |j| @board.each_slice(7).to_a[j][i] }
      end
      (0..5).each do |i|
        tmp << @board.each_slice(7).to_a[i]
      end
      return check_array?(tmp, color)
    end
    
    def check_array?(tmp, color)
      tmp.each do |arraylist| 
        arraylist.each_cons(4).each do |subarray|
          return true if subarray.all? { |node| node.circle_node.color == color}
        end
      end
      return false
    end
    
  end
end

ConnectFour::Game.new
require 'connect_four'

describe ConnectFour do
  
  describe ConnectFour::Board do
    let(:board) { ConnectFour::Board.new }
  
    context "creating object" do
      it "creates a new object" do
        expect(board).to be_truthy
        expect(board).to be_is_a(ConnectFour::Board)
      end
    end
    
    describe "#board" do
      context "grab board" do
        it "returns board" do
          expect(board.board).to be_truthy
          expect(board.board).to be_is_a(Array)
        end
      end
    end
    
    describe "#draw_board" do
      context "retrieve string of the board" do
        it "returns string representation of board" do
          expect(board.draw_board).to be_is_a(String)
        end
      end
    end
  end
end
class Node
  attr_accessor :value, :next
  def initialize(value = nil, next_node = nil)
    @value = value
    @next = next_node
  end
end

class LinkedList
  def initialize
    @node_count = 0
    @head = nil
    @tail = nil
  end

  def append(value)
    node = Node.new(value)
    if @head.nil?
      @head = node
      @tail = node
    else
      @tail.next = node
      @tail = node
    end
    @node_count += 1
  end
  
  def prepend(value)
    node = Node.new(value)
    if @head.nil?
      @head = node
      @tail = node
    else
      node.next = @head
      @head = node
    end
    @node_count += 1
  end
  
  def size; @node_count end
  
  def head; @head end
  
  def tail; @tail end
    
  def at(index)
    node = @head
    index.times do 
      begin
        node = node.next
      rescue
        return nil
      end
    end
    
    node.nil? ? nil : node.value
  end
  
  def pop
    return nil if @head.nil?
    node = @head
    @head = @head.next
    @node_count -= 1
    return node.value
  end
  
  def contains?(value)
    self.size.times do |i| 
      return true if value == self.at(i)
    end
    return false
  end
  
  def find(value)
    self.size.times do |i| 
      return i if value == self.at(i)
    end
    return nil
  end
  
  def to_s
    str = ""
    self.size.times do |i|
      str << "( #{self.at(i)} )"
      str << " -> "
    end
    str << "nil"
    return str
  end
end

class Node
    attr_accessor :x, :y, :parent_node, :child_node
    def initialize(x,y, parent = nil)
      @x = x
      @y = y
      @parent_node = parent
      @child_node = []
    end
end

class Knight
  def movements(parent)
    x, y = parent.x, parent.y
    possible_movements = [[x + 1, y + 2],[x + 2, y + 1],[x + 2, y - 1],[x + 1, y - 2],
                          [x - 1, y + 2],[x - 2, y + 1],[x - 2, y - 1],[x - 1, y - 2]]
    
    possible_movements.each { |move| parent.child_node << move if Board.new.valid_c?(move) }
    parent.child_node.map! { |child| Node.new(child[0], child[1], parent) }
  end
end

class Board
  attr_accessor :board 
  def initialize
    @board = []
    (0..7).to_a.each do |i|
      (0..7).to_a.each do |j|
        @board << [i,j]
      end
    end
  end  
  
  def search_nodes(chessman, start, target)# This tries to see if it can find the target node in the grid.
    queue = []
    queue << start
    loop do
      node = queue.shift
      return node if node.x == target.x && node.y == target.y
      chessman.movements(node).each { |child| queue << child }
    end
  end
  
  def path_from_root(chessman, start, target)
    route = []
    route << [target.x, target.y]
    node = search_nodes(chessman, start, target).parent_node
    until node.nil?
      route.unshift([node.x, node.y])
      node = node.parent_node
    end
    return route
  end
  
  def c_to_index(coordinate)
    self.board.find_index(coordinate)
  end
  
  def i_to_coordinate(index)
    self.board[index]
  end
  
  def valid_c?(coordinate)
    self.board.find_index(coordinate) ? true : false
  end
end
  
def knight_moves(start, target)
  start = Node.new(start[0], start[1])
  target = Node.new(target[0], target[1])
  route = Board.new.path_from_root(Knight.new, start, target)
  puts "You made it in #{route.length - 1} moves! Here's your route:"
  route.each {|tile| puts tile.inspect}
end
class StringCalculator
  def self.add(input)
    input.split(',').inject(0) { |sum, num| sum + num.to_i }
  end
end

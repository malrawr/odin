module TicTacToe
  require 'colorize'
  class Board
    attr_accessor :board
    def initialize
      @board = [[0,1,2],
                [3,4,5],
                [6,7,8]]
      
      @pairs = [[0,3,6],[1,4,7],[2,5,8],[0,1,2],[3,4,5],[6,7,8],[0,4,8],[2,4,6]]
    end
    
    def display
      @board.each do |row| 
        row.each do |cell| 
          print "| #{cell} "
        end
        puts "|" # Creates a newline for each row
      end
    end
    
    def update(i, cell)
      # puts "You can't do this! Pick a different index" if taken?(i)
      @board.each do |row|
        if row.index(i)
          row[row.index(i)] = cell
        end
      end
    end
    
    def victory?
      tmp = check_index
      tmp.each do |x|
        if x.uniq.length == 1
          return true
        end
      end
      return false
    end
    
    def check_index
      tmp = []
      @pairs.each do |x| 
        x.each do |y| 
          if @board.flatten.index(y).nil?
            tmp << @board.flatten[y] 
          else
            tmp << y
          end
        end 
      end
      tmp = tmp.each_slice(3).to_a
    end

    def taken?(i)
      @board.flatten.index(i).nil?
    end
  end
  
  class Player
    attr_reader :name, :char
    def initialize(name, char)
      @name = name
      @char = char
    end
  end
    
  class Game
    attr_reader :players
    def initialize
      hello
      @players = { 
        :player1 => Player.new("Player One", "X".red), 
        :player2 => Player.new("Player Two", "O".blue)
      }
      @board = Board.new
      game_loop
    end
    
    def hello
      "TicTacToe Start!"
    end
    
    def game_loop
      @board.display
      loop {
        @players.each_value { |player| 
          @board.update(prompt_index(player.name), player.char)
          @board.display
          if @board.victory?
            puts "Victory! #{player.name} won!"
            exit
          end
        }
      }
    end
    
    def prompt_index(player)
      answer = nil
      print "#{player}, choose a position: "
      while @board.taken?(answer)
        answer = gets.chomp.to_i
        print "That position is taken, choose another: " if @board.taken?(answer)
      end
      return answer
    end
  end
end

TicTacToe::Game.new

